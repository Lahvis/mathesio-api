<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Repository\CommentRepository;
use App\Repository\ArticleRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/comment", name="comment-")
*/
class CommentController extends AbstractController
{
    /**
     * Creates new comment
     * Data are send via JSON array:
     * 'article' - article id
     * 'user' - user id of author
     * 'text' - main text
     * 
     * @Route("/create", name="create")
     */
    public function create(Request $request, ArticleRepository $articleRepository, UserRepository $userRepository): Response
    {
        // gets JSON data from POST
        $data = json_decode($request->getContent(), true);
        // new Comment instance
        $comment = new Comment();
        // find article by id
        $article = $articleRepository->findOneById($data ['article']);
        // find user by id
        $user = $userRepository->findOneById($data ['user']);
        // sets comment data
        $comment->setArticle($article);
        $comment->setUser($user);
        $comment->setText($data ['text']);
        $comment->setDateTime(new \DateTime('now'));
        // entity manager
        $em = $this->getDoctrine()->getManager();
        // creates SQL query
        $em->persist($comment);
        // send query to DB
        $em->flush();
        // return status code
        return $this->json(['status' => 1]);
    }

    /**
     * Returns comments's data
     * 
     * @Route("/show/{id}", name="show", methods={"GET"})
     */
    public function show($id, CommentRepository $commentRepository): Response
    {
        // finds entity by id
        $comment = $commentRepository->findOneBy(['id' => $id]);
        // get parametrs
        $article = $comment->getArticle()->getTitle();
        $user = $comment->getUser()->getUsername();
        $text = $comment->getText();
        $datetime = $comment->getDateTime();
        // returns data
        return $this->json(['article' => $article, 'user' => $user, 'text' => $text, 'datetime' => $datetime]);
    }

    
}
