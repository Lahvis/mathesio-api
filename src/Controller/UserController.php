<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/user", name="user-")
 */
class UserController extends AbstractController
{
    /**
     * Creates new user
     * Data are send via JSON array:
     * 'name' - new user name
     * 'password' - new user password
     * 
     * @Route("/create", name="create", methods={"POST"})
     */
    public function create(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        // gets JSON data from POST
        $data = json_decode($request->getContent(), true);  
        // new User entity
        $user = new User();
        // set name and password
        $user->setUsername($data ['name']);
        $user->setPassword($passwordEncoder->encodePassword($user, $data['password']));
        // entity manager
        $em = $this->getDoctrine()->getManager();
        // creates SQL query
        $em->persist($user);
        // send query to DB
        $em->flush();
        // return status code
        return $this->json(['status' => 1]);
    }

    /**
     * Returns user's datas
     * In this case only name
     * 
     * @Route("/show/{id}", name="show", methods={"GET"})
     */
    public function show($id, UserRepository $userRepository): Response
    {
        // finds entity by id
        $user = $userRepository->findOneById($id);
        // get parametrs
        $username = $user->getUsername();
        // returns data
        return $this->json(['username' => $username]);
    }
}
