<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/article", name="article-")
 */
class ArticleController extends AbstractController
{
    /**
     * Shows all articles
     * 
     * @Route("/", name="index")
     */
    public function index(ArticleRepository $articleRepository): Response
    {
        // get articles orders by date time
        $articles = $articleRepository->findBy([], ['date_time' => 'DESC']);
        // sort instances into array
        $array = array();
        foreach ($articles as $article) {
            array_push($array, ['id' => $article->getId(), 'title' => $article->getTitle(), 'user' => $article->getUser()->getUsername(), 'datetime' => $article->getDateTime()]);
        }
        // returns data array
        return $this->json($array);
    }
    
    /**
     * Creates new article
     * Data are send via JSON array:
     * 'user' - user id of author
     * 'title' - title od article
     * 'text' - main text
     * 
     * @Route("/create", name="create", methods={"POST"})
     */
    public function create(Request $request, UserRepository $userRepository): Response
    {
        // gets JSON data from POST
        $data = json_decode($request->getContent(), true);
        // new Article entity
        $article = new Article();
        // find user by id
        $user = $userRepository->findOneById($data ['user']);
        // sets article data
        $article->setUser($user);
        $article->setTitle($data ['title']);
        $article->setText($data ['text']);
        $article->setDateTime(new \DateTime('now'));
        // entity manager
        $em = $this->getDoctrine()->getManager();
        // creates SQL query
        $em->persist($article);
        // send query to DB
        $em->flush();
        // return status code
        return $this->json(['status' => 1]);
    }

    /**
     * Returns article's data
     * 
     * @Route("/show/{id}", name="show", methods={"GET"})
     */
    public function show($id, ArticleRepository $articleRepository): Response
    {
        // finds entity by id
        $article = $articleRepository->findOneBy(['id' => $id]);
        // get parametrs
        $user = $article->getUser()->getUsername();
        $title = $article->getTitle();
        $text = $article->getText();
        $datetime = $article->getDateTime();
        // returns data
        return $this->json(['user' => $user, 'title' => $title, 'text' => $text, 'datetime' => $datetime]);
    }

    /**
     * Returns all article's comments
     * 
     * @Route("/comments/{id}",name="comments", methods={"GET"})
     */
    public function comments ($id, ArticleRepository $articleRepository): Response
    {
        // finds entity by id
        $article = $articleRepository->findOneBy(['id' => $id]);
        // returns comments
        $comments = $article->getComments();
        $array = array();
        foreach ($comments as $comment) {
            array_push($array, ['id' => $comment->getId(), 'user' => $comment->getUser()->getUsername(), 'text' => $comment->getText(), 'datetime' => $comment->getDateTime()]);
        }
        // returns data array
        return $this->json($array);

    }
}

